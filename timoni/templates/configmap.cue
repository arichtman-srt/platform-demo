package templates

import (
	"encoding/yaml"
	"strings"
	"uuid"

	corev1 "k8s.io/api/core/v1"
)

#ConfigMap: corev1.#ConfigMap & {
	_config:    #Config
	apiVersion: "v1"
	kind:       "ConfigMap"
	metadata: {
		name:      "\(_config.metadata.name)-\(_checksum)"
		namespace: _config.metadata.namespace
		labels:    _config.metadata.labels
	}
	immutable: true
	let _checksum = strings.Split(uuid.SHA1(uuid.ns.DNS, yaml.Marshal(data)), "-")[0]
	data: {
		"s3_proxy.conf":
			"""
			log_format  s3proxy  '[$time_local] $host "$upstream_http_x_request_id" $status "$request" '
							'$upstream_addr $request_time $upstream_response_time "$sent_http_content_type" $body_bytes_sent '
							'$remote_addr $cookie__session_id "$http_user_agent" "$http_referer" $ssl_protocol/$ssl_cipher';

			server {
				include       /etc/nginx/mime.types;
				default_type  application/octet-stream;

				sendfile        on;
				keepalive_timeout  65;

				autoindex off;

				listen 80 default_server;

				server_name _;

				access_log /dev/stdout s3proxy;
				error_log stderr;

				location = /ping {
					default_type text/plain;
					sendfile off;
					return 200;
				}

				location / {
					resolver 9.9.9.9 ipv6=off;
					proxy_pass https://timoni-\(_config.metadata.name)-srt.s3.amazonaws.com$uri$is_args$args;
				}
			}
			"""
	}
}
