package templates

import (
	timoniv1 "timoni.sh/core/v1alpha1"
)

// Config defines the schema and defaults for the Instance values.
#Config: {
	// Runtime version info
	moduleVersion!: string
	kubeVersion!:   string

	// Metadata (common to all resources)
	metadata: timoniv1.#Metadata & {#Version: moduleVersion}

	// Label selector (common to all resources)
	selector: timoniv1.#Selector & {#Name: metadata.name}

	// Deployment
	replicas: *1 | int & >0

	// Container
	image!:           timoniv1.#Image
	imagePullPolicy:  *"IfNotPresent" | string

	// Service
	service: port: *80 | int & >0 & <=65535

}

// Instance takes the config values and outputs the Kubernetes objects.
#Instance: {
	config: #Config

	objects: {
		svc: #Service & {_config:        config}
		cm:  #ConfigMap & {_config:      config}
		ing: #Ingress & {_config: config}
		bucket: #Bucket & {_config: config}
		bucketpab: #BucketPublicAccessBlock & {_config: config}
		bucketpol: #BucketPolicy & {_config: config}
		bucketobj: #BucketObject & {_config: config}

		deploy: #Deployment & {
			_config: config
			_cmName: objects.cm.metadata.name
		}
	}
}
