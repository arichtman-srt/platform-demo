package templates

import (
	s3bucketpublicaccessblockv1beta1 "s3.aws.upbound.io/bucketpublicaccessblock/v1beta1"
)

#BucketPublicAccessBlock: s3bucketpublicaccessblockv1beta1.#BucketPublicAccessBlock & {
	_config:    #Config
	metadata:   _config.metadata
	spec:       s3bucketpublicaccessblockv1beta1.#BucketPublicAccessBlockSpec & {
		forProvider : {
			region: "eu-west-1"
			bucketRef: { name: "timoni-\(metadata.name)-srt" }
		}
	}
}
