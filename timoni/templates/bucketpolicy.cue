package templates

import (
	s3bucketpolicyv1beta1 "s3.aws.upbound.io/bucketpolicy/v1beta1"
	"encoding/json"
)

#BucketPolicy: s3bucketpolicyv1beta1.#BucketPolicy & {
	_config:    #Config
	metadata:   _config.metadata
	spec:       s3bucketpolicyv1beta1.#BucketPolicySpec & {
		forProvider : {
			region: "eu-west-1"
			bucket: "timoni-platform-demo-srt"
			policy: json.Marshal({
	Version: "2012-10-17"
	Statement: [{
			Sid: "Statement1",
			Principal: "*",
			Effect: "Allow",
			Action: [
				"s3:ListBucket",
				"s3:GetObject"
			],
			Resource: [
				"arn:aws:s3:::timoni-\(metadata.name)-srt/*",
				"arn:aws:s3:::timoni-\(metadata.name)-srt"
			]
		}
		]
})
		}
	}
}
