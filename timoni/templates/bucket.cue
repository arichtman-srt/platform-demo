package templates

import (
	s3bucketv1beta1 "s3.aws.upbound.io/bucket/v1beta1"
)

#Bucket: s3bucketv1beta1.#Bucket & {
	_config:    #Config
	// It's unclear why this clashes, perhaps CRDs spec their own api version
	// apiVersion: "v1beta1"
	kind:       "Bucket"
	metadata:   _config.metadata
	spec:       s3bucketv1beta1.#BucketSpec & {
		forProvider : {
			region: "eu-west-1"
			tags: {
				Name: "timoni-\(metadata.name)-srt"
				// I'd like to be able to feed-through and merge labels here
				// e.g. _config.metadata.labels
			}
		}
	}
}
