package templates

import (
	s3bucketobjectv1beta1 "s3.aws.upbound.io/bucketobject/v1beta1"
)

#BucketObject: s3bucketobjectv1beta1.#BucketObject & {
	_config:    #Config
	metadata:   _config.metadata
	spec:       s3bucketobjectv1beta1.#BucketObjectSpec & {
		forProvider : {
			region: "eu-west-1"
			bucketRef: { name: "timoni-\(metadata.name)-srt" }
			key: "test-object"
			content: "This is a Timoni object"
		}
	}
}
