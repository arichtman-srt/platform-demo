package templates

import (
	networkingv1 "k8s.io/api/networking/v1"
)

#Ingress: networkingv1.#Ingress & {
	_config:    #Config
	// Unclear why just "v1" fails and removing it entirely causes it to get skipped
	apiVersion: "networking.k8s.io/v1"
	kind:       "Ingress"
	metadata:   _config.metadata
	spec:       networkingv1.#IngressSpec & {
		ingressClassName: "alb-private"
		rules: [
			{
				host: "timoni.poc.one-platform.srt.global"
				http: paths: [
					{
						backend: service: {
							name: "timoni"
							port: number: _config.service.port
							}
						path: "/*"
						pathType: "ImplementationSpecific"
					}
				]
			}
		]
		tls: [
			{
				hosts: [
					"timoni.poc.one-platform.srt.global"
				]
			}
		]
	}
}
