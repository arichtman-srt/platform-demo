# platform-demo

A [timoni.sh](http://timoni.sh) module for deploying platform-demo to Kubernetes clusters.

## Install

To create an instance using the default values:

```shell
timoni -n default apply platform-demo oci://<container-registry-url>
```

To change the [default configuration](#configuration),
create one or more `values.cue` files and apply them to the instance.

For example, create a file `my-values.cue` with the following content:

```cue
values: {
  image: {
    tag: "1.25"
  }
}
```

And apply the values with:

```shell
timoni -n default apply platform-demo oci://<container-registry-url> \
--values ./my-values.cue
```

## Uninstall

To uninstall an instance and delete all its Kubernetes resources:

```shell
timoni -n default delete platform-demo
```

## Configuration

### General values

| Key                          | Type                                    | Default                    | Description                                                                                                                                  |
|------------------------------|-----------------------------------------|----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| `image: tag:`                | `string`                                | `latest`                   | Container image tag                                                                                                                          |
| `image: digest:`             | `string`                                | `""`                       | Container image digest, takes precedence over `tag` when specified                                                                           |
| `image: repository:`         | `string`                                | `nignx`                    | Container image repository                                                                                                                   |
| `image: pullPolicy:`         | `string`                                | `IfNotPresent`             | [Kubernetes image pull policy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy)                                     |

#### Recommended values

Comply with the restricted [Kubernetes pod security standard](https://kubernetes.io/docs/concepts/security/pod-security-standards/):

```cue
values: {
  podSecurityContext: {
    runAsUser:  65532
    runAsGroup: 65532
    fsGroup:    65532
  }
  securityContext: {
    allowPrivilegeEscalation: false
    readOnlyRootFilesystem:   false
    runAsNonRoot:             true
    capabilities: drop: ["ALL"]
    seccompProfile: type: "RuntimeDefault"
  }
}
```
