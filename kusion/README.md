# Kusion

This one's more of an investment as it's a continuous delivery platform.
The docs don't really cover using the `AppConfiguration` schema or their framework with_out_ the platform.
I expect one could do it with vanilla KCL and a touch of wrapping.
Worth noting it's in competition with the _Open Application Model_, though both appear to be pets of their respective companies.
KCL is a Linux Foundation project, while Kusion itself claims to be CNCF but is only in the landscape, not actually incubating or anything.

[Docs](https://www.kusionstack.io/)
