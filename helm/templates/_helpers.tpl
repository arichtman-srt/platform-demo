{{/*
Expand the name of the chart.
*/}}
{{- define "helm-demo.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "helm-demo.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "helm-demo.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "helm-demo.labels" -}}
helm.sh/chart: {{ include "helm-demo.chart" . }}
{{ include "helm-demo.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "helm-demo.selectorLabels" -}}
app.kubernetes.io/name: {{ include "helm-demo.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the FQDN for external access
*/}}
{{- define "helm-demo.accessFQDN" -}}
{{- include "helm-demo.fullname" . -}}.{{- .Values.clusterFQDN -}}
{{- end }}

{{/*
Canonically unique name for AWS resources
*/}}
{{- define "helm-demo.awsName" -}}
{{- include "helm-demo.name" . -}}-platform-demo-srt
{{- end }}

{{/*
Random-suffix name for the config map to trigger redeployments
Should really be content-addressed but that's cyclical if .Values is fed in
*/}}
{{- define "helm-demo.randomSlug" -}}
{{- include "helm-demo.name" . -}}-{{ randAlphaNum 5 | lower }}
{{- end }}

{{/*
Content-addressed value to trigger redeployment.
We truncate it at 8 just to preserve legibility.
Yes, we're increasing the chance of clashes.
*/}}
{{- define "helm-demo.configMapContentHash" -}}
{{ include (print $.Template.BasePath "/configMap.yaml") . | sha256sum | trunc 8 }}
{{- end }}
