# Tanka

[Docs](https://tanka.dev/)

Jsonnet is a bit nicer than Cue lang.
CRD support is there but limited. You don't get constructors.
Still some boilerplate to wire stuff together.
Doesn't look like any nice way to weave secrets in.
Helm support is nice though you do have to vendor in charts and it's a bit verbose the way it names things.
