# Platform Demo

Demo repository implementing the same single application but using different frameworks/tools.

![App Diagram](./app-diagram.svg)

Prerequisites:

- Kubernetes cluster with namespace and (basically) admin access
- CLI tool access e.g. kubectl, helm etc

## Raw Manifests

Good place to start.
Contains absolutely no-frills YAML manifests.

`kubectl apply -f raw-manifests`

## Helm

Very popular.
I've removed a lot of the articulation that comes from `helm create` as it's not the focus of this repo.

`helm install --upgrade helm ./helm`

## Timoni

Templates and applies OK-ish.
Some issue with bucket regions that I've seen with Terraform before.

- Uses OCI artifacts
- Great schema definition and composition
- Heavily Go-lang invested
- Have to vendor in CRDs (may be resolvable with the packaging system)
- Cue lang/modules not pick-up-and-use
- Some foibles about multiline strings

```shell
timoni mod vendor crd -f https://raw.githubusercontent.com/upbound/provider-aws/main/package/crds/s3.aws.upbound.io_buckets.yaml
timoni mod vendor crd -f https://github.com/upbound/provider-aws/raw/main/package/crds/s3.aws.upbound.io_bucketpolicies.yaml
timoni mod vendor crd -f https://github.com/upbound/provider-aws/raw/main/package/crds/s3.aws.upbound.io_bucketpublicaccessblocks.yaml
timoni mod vendor crd -f https://github.com/upbound/provider-aws/blob/main/package/crds/s3.aws.upbound.io_bucketobjects.yaml

timoni build platform-demo .
timoni apply platform-demo . --wait false

timoni list
```

## The rest

Not worked on yet.
